# Readable

Readable is a bash script which makes tables readable.

It reads unreadable table from stdin and prints in 
readable format to stdout.

For example
```
$ docker ps
CONTAINER ID        IMAGE                                 COMMAND                  CREATED             STATUS
PORTS                               NAMES
ab38f3b25555        repo/image1:latest                    "/init.sh --enable-j…"   34 hours ago        Up 34 hours
127.0.0.1:12345->8080/tcp           container_1
aa66c9177555        repo2/server                          "/entrypoint.sh"         34 hours ago        Up 34 hours
0.0.0.0:50000->22/tcp               container_2
a42d1594b555        repo3/container:latest                "/start.sh"              3 months ago        Up 3 days
192.168.0.4:389->389/tcp, 636/tcp   container_3
```
Which is a bit of unreadable. We can make it readable with
```
$ docker ps | readable
RECORD 1 ===============================================
CONTAINER_ID     ab38f3b25555
IMAGE            repo/image1:latest
COMMAND          "/init.sh --enable-j…"
CREATED          34 hours ago
STATUS           Up 34 hours
PORTS            127.0.0.1:12345->8080/tcp
NAMES            container_1

RECORD 2 ===============================================
CONTAINER_ID     aa66c9177555
IMAGE            repo2/server
COMMAND          "/entrypoint.sh"
CREATED          34 hours ago
STATUS           Up 34 hours
PORTS            0.0.0.0:50000->22/tcp
NAMES            container_2

RECORD 3 ===============================================
CONTAINER_ID     a42d1594b555
IMAGE            repo3/container:latest
COMMAND          "/start.sh"
CREATED          3 months ago
STATUS           Up 3 days
PORTS            192.168.0.4:389->389/tcp, 636/tcp
NAMES            container_3
```

If there is no headers row you can simply set number of
columns via first positional argumen (f. ex. ```readable 5```)
